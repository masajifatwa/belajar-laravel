<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





route::get('/', 'HomeController@home');
route::get('/register','AuthController@register');
route::post('/selamat','AuthController@selamat');
route::get('/master', function(){
    return view('master');
});
route::get('/datatable', function(){
    return view('datatable');
});

route::get('/pertanyaan/create','PertanyaanController@create');
route::post('/pertanyaan','PertanyaanController@store');
route::get('/pertanyaan','PertanyaanController@index');
route::get('/pertanyaan/{pertanyaan_id}','PertanyaanController@show');
route::get('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');
route::put('/pertanyaan/{pertanyaan_id}','PertanyaanController@update');
route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');
@extends ('master')
@section ('content')
<form method='post' action="/pertanyaan/{{$post->id}}">
    @csrf
    @method ('PUT')
    <div class="card-header">
        <h3 class="card-title">Edit pertanyaan</h3>
    </div>

    <div class="card-body">

        <div class="form-group">
            <label>Judul</label>
            <input type="text" class="form-control" placeholder="Masukan Judul" name="judul" value="{{$post->judul}}">
            @error('judul')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror

        </div>
        <div class="form-group">
            <label>Isi</label>
            <textarea class="form-control" rows="3" placeholder="Tuliskan ISI..." name="isi" >{{$post->isi}}</textarea>
            @error('isi')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror

        </div>
        
        <div class="form-group">
            <label>ID Anda</label>
            <input type="text" class="form-control"  placeholder="Masukan ID" name="profileid" value="{{$post->profile_id}}">
        </div>
        <div class="form-group">
            <label>ID Jawaban Anda</label>
            <input type="text" class="form-control"  placeholder="ID Jawaban" name="jawabanid"value="{{$post->jawaban_id}}">
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection

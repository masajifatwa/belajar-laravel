@extends ('master')
@section ('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Table Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                  @endif
                  <a href="/pertanyaan/create" class="btn btn-primary">Tambah Data</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse($post as $key => $post)
                    <tr>
                      <td>{{$key + 1}}</td>
                      <td>{{$post->judul}}</td>
                      <td>{{$post->isi}}</td>
                    
                      <td style="display:flex">
                          <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">Show</a>
                          <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                          <form action="/pertanyaan/{{$post->id}}" method="post">
                          @csrf
                          @method ('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                      </td>
                    </tr>
                    @empty
                    <p>Data Kosong</P>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!--
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
-->
            </div>
            @endsection
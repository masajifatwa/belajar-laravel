@extends ('master')
@section ('content')
<form method='post' action="/pertanyaan">
    @csrf
    <div class="card-header">
        <h3 class="card-title">Create pertanyaan</h3>
    </div>

    <div class="card-body">

        <div class="form-group">
            <label>Judul</label>
            <input type="text" class="form-control" placeholder="Masukan Judul" name="judul">
            @error('judul')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror

        </div>
        <div class="form-group">
            <label>Isi</label>
            <textarea class="form-control" rows="3" placeholder="Tuliskan ISI..." name="isi"></textarea>
            @error('isi')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror

        </div>
 
   
        
        <div class="form-group">
            <label>ID Anda</label>
            <input type="text" class="form-control"  placeholder="Masukan ID" name="profileid">
        </div>
        <div class="form-group">
            <label>ID Jawaban Anda</label>
            <input type="text" class="form-control"  placeholder="ID Jawaban" name="jawabanid">
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection

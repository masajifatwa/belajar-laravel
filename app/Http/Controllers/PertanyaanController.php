<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pertanyaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'judul'=>'required',
            'isi'=>'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul"=>$request['judul'],
            "isi"=>$request['isi'],
            "profile_id"=>$request['profileid'],
            "jawaban_id"=>$request['jawabanid']
        ]);
        return redirect('/pertanyaan')->with('success', 'pertanyaan berhasil di post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($pertanyaan_id)
    {
        $post = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($pertanyaan_id)
    {
        $post = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pertanyaan_id)
    {
        $request->validate([
            'judul'=>'required',
            'isi'=>'required'
        ]);

        $query = DB::table('pertanyaan')
        ->where('id', $pertanyaan_id)
        ->update([
            "judul"=>$request['judul'],
            "isi"=>$request['isi'],
            "profile_id"=>$request['profileid'],
            "jawaban_id"=>$request['jawabanid']
        ]);
        return redirect('/pertanyaan')->with('success', 'berhasil updaate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($pertanyaan_id)
    {
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan')->with('success', 'berhasil Hapus');
    }
}
